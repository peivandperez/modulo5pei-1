package cl.vivimos.testing.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


import cl.vivimos.testing.model.dto.SalesDTO;
import cl.vivimos.testing.model.dto.UsersDTO;

import cl.vivimos.testing.service.UsersService;

@Controller
public class UsersController {
	@Autowired
	UsersService usersService;
	
	@Autowired
	private PasswordEncoder bCryptPasswordEncoder;
	
	@RequestMapping("/createUser")
	public  String Insert(HttpServletRequest req){
	 int rows=0;
	 int rowsroles=0;
	 try {

		
	UsersDTO usersDTO = new UsersDTO();
	
	String password = req.getParameter("password");
	usersDTO.setPassword(bCryptPasswordEncoder.encode(password));
	usersDTO.setUsername(req.getParameter("username"));
	usersDTO.setEnabled(true);
	usersDTO.setRole("User");
	
	rows=usersService.insertUsers(usersDTO);
	rowsroles=usersService.insertRoles(usersDTO);

	
	 } catch (Exception e) {
			e.printStackTrace(); 
		 
		}
	 
		return "insert";
	}
	
	  @RequestMapping(value = "/userdetails", method = RequestMethod.GET)
	  @ResponseBody
	  public String currentUserName(Authentication authentication) {
		  System.out.println("username: " + authentication.getName()+" authority: "+ authentication.getAuthorities());
	     return authentication.getName() + " Authentication: " + authentication.getAuthorities();
	     
	  }
	  
	  @RequestMapping(value="/listUsers")
		public @ResponseBody List<UsersDTO> ajaxList(HttpServletRequest req,HttpServletResponse res){
			return usersService.listUsers();
		}
	  }
	
	

