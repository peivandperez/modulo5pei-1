package cl.vivimos.testing.controller;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Servlet implementation class WebAppController
 */
@Controller("/WebAppController")
public class WebAppController {
	
		@RequestMapping("/")
		public String getHome(){
			return "home";
		}
	
		@RequestMapping("/login")
		public String getLogin(){
			return "login";
		}
		
		@RequestMapping("/index")
		public String getIndex(){
			return "index";
		}
		@RequestMapping("sales")
		public String getSales() {
			return "sales";
			
		}
		
		@RequestMapping("/403")
		public String get403(){
			return "403";
		}

		@RequestMapping("/userManagment")
		public String getUserManagment(){
			return "UserManagment";
		}
		
		@RequestMapping("/insert")
		public String getInsert(){
			return "insert";
		}
		

}
