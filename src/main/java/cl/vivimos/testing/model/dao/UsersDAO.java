package cl.vivimos.testing.model.dao;

import java.util.List;

import cl.vivimos.testing.model.dto.UsersDTO;

public interface UsersDAO {
 public int insertUsers(UsersDTO usersDTO);
 public UsersDTO getUsers(int id_user);
 public int updateUsers(UsersDTO usersDTO);
 public int deleteUsers(int id_user);
 public List<UsersDTO> listUsers();
 
 
 public int insertRoles(UsersDTO usersDTO);
 public UsersDTO getRoles(int id_role);
 public int updateRoles(UsersDTO usersDTO);
 public int deleteRoles(int id_role);
 public List<UsersDTO> listRoles();
}
