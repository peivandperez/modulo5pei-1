package cl.vivimos.testing.model.dao;

import java.util.List;

import cl.vivimos.testing.model.dto.SalesDTO;

public interface SalesDAO {


public int insert(SalesDTO salesDTO); //Es tipo int debido a que la operación devuelve la cantidad de filas modificadas, en este caso queremos realizar sólo una modificacion. Recibe un dato tipo SalesDTO con nombre SalesDTO
public SalesDTO get(int id); //como es un read, vamos a obtener un dato de SalesDTO, un sólo sale y lo manejamos a nivel DTO,
public int update(SalesDTO salesDTO); //recibe un dato tipo SalesDTO con nombre SalesDTO
public int delete(int id);//
public List<SalesDTO> list();// una lista de tipo SalesDTO llamado list
}
