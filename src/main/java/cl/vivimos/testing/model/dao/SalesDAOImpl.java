package cl.vivimos.testing.model.dao;

import java.sql.PreparedStatement;
import java.sql.Types;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import cl.vivimos.testing.model.dto.SalesDTO;

@Repository
public class SalesDAOImpl implements SalesDAO {
	private String insert="INSERT INTO sales (item,quantity,price,sale_date) VALUES(:item, :quantity, :price, :sale_date)";
	private String select="SELECT * FROM sales WHERE id=?";
	private String update="UPDATE sales SET item= :item, quantity= :quantity, price= :price, sale_date= :sale_date WHERE id= :id";
	private String delete="DELETE FROM sales WHERE id=?";
	private String list="SELECT * FROM sales";
	

	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired 
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	

	@Override
	public int insert(SalesDTO salesDTO) {
		

	
		int rows =0;
	
		 
		try {
		
	
			MapSqlParameterSource params =new MapSqlParameterSource();
		
			params.addValue("item", salesDTO.getItem(), Types.VARCHAR);
			params.addValue("quantity", salesDTO.getQuantity(), Types.INTEGER);
			
			if (salesDTO.getPrice() !=null) {
				params.addValue("price", salesDTO.getPrice(), Types.DOUBLE);
			}else {
			params.addValue("price", salesDTO.getPrice(), Types.NULL);
			}

			if (salesDTO.getSale_date() !=null) {
				params.addValue("sale_date", salesDTO.getSale_date(),Types.DATE);
			}else {
				params.addValue("sale_date", salesDTO.getSale_date(), Types.NULL);
			}
			
			
			rows = namedParameterJdbcTemplate.update(insert, params);
			
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		return rows;
		
	}

	@Override
	public SalesDTO get(int id) {
		Object args[]= {id};
		SalesDTO salesDTO;
		try {
			salesDTO= jdbcTemplate.queryForObject(select, args, BeanPropertyRowMapper.newInstance(SalesDTO.class));
			
		} catch (Exception e) {
			salesDTO=null;
			e.printStackTrace();
		}
		return salesDTO; 
	}

	@Override
	public int update(SalesDTO salesDTO) {
		int rows =0;
	
		MapSqlParameterSource params =new MapSqlParameterSource();
		
		params.addValue("item", salesDTO.getItem(), Types.VARCHAR);
		params.addValue("quantity", salesDTO.getQuantity(), Types.INTEGER);
		if (salesDTO.getPrice() !=null) {
			params.addValue("price", salesDTO.getPrice(), Types.DOUBLE);
		}else {
		params.addValue("price", salesDTO.getPrice(), Types.NULL);
		}
		if (salesDTO.getSale_date() !=null) {
			params.addValue("sale_date", salesDTO.getSale_date(),Types.DATE);
		}else {
			params.addValue("sale_date", salesDTO.getSale_date(), Types.NULL);
		}
		params.addValue("id", salesDTO.getId(), Types.INTEGER);
	
		
			try {
				
				rows = namedParameterJdbcTemplate.update(update, params);
				
					
			 } catch (Exception e) {
					e.printStackTrace();
				
				}
				 	
			return rows;
	}
	


	@Override
	public int delete(int id) {
	int rows=0;
	
		Object args[]= {id};
	try {
			rows= jdbcTemplate.update(delete,args);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rows; 
	}

	@Override
	public List<SalesDTO> list() {
	//devuelve un tipo List SalesDTO
	//vamos a hacer un mapeo para la fila. Se ocupa el rowMapper -->se ocupa clase BeanPropertyRowMapper, convierte una clase a un objeto, así
	//jdbcTemplate sabe que tiene que devolver un dato tipo SalesDTO
	//como esta consulta no recibe argumentos dado que es un Select * ; por lo tanto se quita el "args".	
		List<SalesDTO> listSales= jdbcTemplate.query(list, BeanPropertyRowMapper.newInstance(SalesDTO.class));
		return listSales;
	}

}
