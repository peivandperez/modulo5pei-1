package cl.vivimos.testing.model.dao;

import java.sql.Types;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import cl.vivimos.testing.model.dto.UsersDTO;



@Repository
public class UsersDAOImpl implements UsersDAO{
	private String insertUsers="INSERT INTO users (username,password,enabled) VALUES(:username, :password, :enabled)";
	private String selectUsers="SELECT username FROM users WHERE id_user= :id_user";
	private String updateUsers="UPDATE sales SET item= :item, quantity= :quantity, price= :price, sale_date= :sale_date WHERE id= :id";
	private String deleteUsers="DELETE FROM sales WHERE id= :id";
	private String listUsersq="SELECT * FROM users";
	
	
	private String insertRoles="INSERT INTO users_roles (users_id_user, roles_id_role) "
			+ "VALUES("
			+ "(Select u.id_user from users u where u.username= :username) "
			+ ",(Select r.id_role from roles r where r.role= :role)"
			+ ")";
			
		
		private String selectRoles="SELECT username FROM users WHERE id_user= :id_user";

	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired 
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	

	@Override
	public int insertUsers(UsersDTO usersDTO) {
		int rows =0;
		
		try {
		
	
			MapSqlParameterSource params =new MapSqlParameterSource();
		
			params.addValue("username", usersDTO.getUsername(), Types.VARCHAR);
			params.addValue("password", usersDTO.getPassword(), Types.VARCHAR); 
			params.addValue("enabled", usersDTO.isEnabled(),Types.BOOLEAN);
			
			rows = namedParameterJdbcTemplate.update(insertUsers, params);
			
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		return rows;
	}

	@Override
	public UsersDTO getUsers(int id_user) {
		Object args[]= {id_user};
		UsersDTO usersDTO;
		try {
			usersDTO= jdbcTemplate.queryForObject(selectUsers, args, BeanPropertyRowMapper.newInstance(UsersDTO.class));
			
		} catch (Exception e) {
			usersDTO=null;
			e.printStackTrace();
		}
		return usersDTO; 
	}
	
	@Override
	public int updateUsers(UsersDTO usersDTO) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int deleteUsers(int id_user) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<UsersDTO> listUsers() {
		List<UsersDTO> listUsers= jdbcTemplate.query(listUsersq, BeanPropertyRowMapper.newInstance(UsersDTO.class));
		return listUsers;
		
	}

	@Override
	public int insertRoles(UsersDTO usersDTO) {
int rows =0;
		
		try {
		
	
			MapSqlParameterSource params =new MapSqlParameterSource();

			params.addValue("username", usersDTO.getUsername(), Types.VARCHAR);
			params.addValue("role", usersDTO.getRole(), Types.VARCHAR);
		
			
			rows = namedParameterJdbcTemplate.update(insertRoles, params);
			
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		return rows;
	}
	

	@Override
	public UsersDTO getRoles(int id_role) {
		Object args[]= {id_role};
		UsersDTO usersDTO;
		try {
			usersDTO= jdbcTemplate.queryForObject(selectRoles, args, BeanPropertyRowMapper.newInstance(UsersDTO.class));
			
		} catch (Exception e) {
			usersDTO=null;
			e.printStackTrace();
		}
		return usersDTO; 
	}

	@Override
	public int updateRoles(UsersDTO usersDTO) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int deleteRoles(int id_role) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<UsersDTO> listRoles() {
		// TODO Auto-generated method stub
		return null;
	}

}
