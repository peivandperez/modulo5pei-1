package cl.vivimos.testing.model.dto;

import java.sql.Date;

public class SalesDTO {
private Integer id; // ocupamos la clase Integer, porque JDBC template cuando el dato es nulo y nativo de Java, se cae. 	
private String item;
private Integer quantity;
private Double price;
private Date sale_date;

public SalesDTO(String item, Integer quantity, Double amount, Date sale_date) {
	this.item = item;
	this.quantity = quantity;
	this.price = amount;
	this.sale_date = sale_date;
}

public SalesDTO() {
	
}
public Integer getId() {
	return id;
}
public void setId(Integer id) {
	this.id = id;
}
public String getItem() {
	return item;
}
public void setItem(String item) {
	this.item = item;
}
public Integer getQuantity() {
	return quantity;
}
public void setQuantity(Integer quantity) {
	this.quantity = quantity;
}
public Double getPrice() {
	return price;
}
public void setPrice(Double amount) {
	this.price = amount;
}
public Date getSale_date() {
	return sale_date;
}
public void setSale_date(Date sale_date) {
	this.sale_date = sale_date;
}



}

