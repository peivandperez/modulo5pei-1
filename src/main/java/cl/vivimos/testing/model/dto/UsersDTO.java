package cl.vivimos.testing.model.dto;


public class UsersDTO {

	private Integer id_user;
	private String username;
	private String password;
	private boolean enabled;
	
	private Integer id_role;
	private String role;
	
	
	
	public Integer getId_role() {
		return id_role;
	}
	public void setId_role(Integer id_role) {
		this.id_role = id_role;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public Integer getId_user() {
		return id_user;
	}
	public void setId_user(Integer id_user) {
		this.id_user = id_user;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	
	public UsersDTO() {
	}
	public UsersDTO(String username, String password, boolean enabled, String role) {
		this.username = username;
		this.password = password;
		this.enabled = enabled;
		this.role = role;
	
	}
	@Override
	public String toString() {
		return "UsersDTO [id_user=" + id_user + ", username=" + username + ", password=" + password + ", enabled="
				+ enabled + ", id_role=" + id_role + ", role=" + role + "]";
	}
	
	
}
