package cl.vivimos.testing.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import cl.vivimos.testing.model.dao.UsersDAO;
import cl.vivimos.testing.model.dto.UsersDTO;

@Service

public class UsersServiceImpl implements UsersService {
	
	//hacemos uso de la interfaz UsersDAO
	@Autowired
	UsersDAO usersDAO;

	@Override
	public int insertUsers(UsersDTO usersDTO) {
		return usersDAO.insertUsers(usersDTO);
	}

	@Override
	public UsersDTO getUsers(int id_user) {
		return usersDAO.getUsers(id_user);
	}

	@Override
	public int updateUsers(UsersDTO usersDTO) {
		return usersDAO.updateUsers(usersDTO);
	}

	@Override
	public int deleteUsers(int id_user) {
		return usersDAO.deleteUsers(id_user);
	}

	@Override
	public List<UsersDTO> listUsers() {
		return usersDAO.listUsers();
	}

	@Override
	public int insertRoles(UsersDTO usersDTO) {
		return usersDAO.insertRoles(usersDTO);
	}

	@Override
	public UsersDTO getRoles(int id_role) {
		return usersDAO.getRoles(id_role);
	}

	@Override
	public int updateRoles(UsersDTO usersDTO) {
		return usersDAO.updateRoles(usersDTO);
	}

	@Override
	public int deleteRoles(int id_role) {
		return usersDAO.deleteRoles(id_role);
		
	}

	@Override
	public List<UsersDTO> listRoles() {
		return usersDAO.listRoles();
	}
	


	
	  
}
