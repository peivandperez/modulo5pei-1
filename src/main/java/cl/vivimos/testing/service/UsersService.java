package cl.vivimos.testing.service;

import java.util.List;

import cl.vivimos.testing.model.dto.UsersDTO;

public interface UsersService {
	 public int insertUsers(UsersDTO usersDTO);
	 public UsersDTO getUsers(int id_user);
	 public int updateUsers(UsersDTO usersDTO);
	 public int deleteUsers(int id_user);
	 public List<UsersDTO> listUsers();
	 
	 public int insertRoles(UsersDTO UsersDTO);
	 public UsersDTO getRoles(int id_role);
	 public int updateRoles(UsersDTO usersDTO);
	 public int deleteRoles(int id_role);
	 public List<UsersDTO> listRoles();
}
