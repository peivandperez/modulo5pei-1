package cl.vivimos.testing.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.vivimos.testing.model.dao.SalesDAO;
import cl.vivimos.testing.model.dto.SalesDTO;

@Service
public class SalesServiceImpl implements SalesService {

	//hacemos uso de la interfaz SalesDAO
	@Autowired
	SalesDAO salesDAO; 
	
	public SalesServiceImpl() {
		 
	}

	//implementamos método lista.Se implementa el método definido en la interfaz.
	public List<SalesDTO> list(){
		return salesDAO.list();
	}

	@Override
	public int insert(SalesDTO salesDTO) {
		return salesDAO.insert(salesDTO);
	}

	@Override
	public SalesDTO get(int id) {
	return salesDAO.get(id);
	
	}

	@Override
	public int update(SalesDTO salesDTO) {
	return salesDAO.update(salesDTO);
	}

	@Override
	public int delete(int id) {
		return salesDAO.delete(id);
	}
	
}
