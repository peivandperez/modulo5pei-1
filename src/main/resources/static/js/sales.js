function SalesController(option){
	$("#msg").hide();
	$("#msg").removeClass("alert-success").addClass("alert-danger");
	var token = $("meta[name='_csrf']").attr("content");
	switch(option){
	
	case "list":
		$.ajax({
			type : "post",
			headers: {"X-CSRF-TOKEN": token}, //send CSRF token in header
			url : "/sales/list",
			success : function(res){
				$("#salesTable").bootstrapTable('load',res);
				$("#salesTable tbody").on('click','tr', function(){
					$("#id").val($(this).find("td:eq(0)").text());
					$("#item").val($(this).find("td:eq(1)").text());
					$("#quantity").val($(this).find("td:eq(2)").text());
					$("#price").val($(this).find("td:eq(3)").text());
					$("#sale_date").val($(this).find("td:eq(4)").text());
					$("#myModal").click()
				});
				$("#myModal").modal({show:true})
			},
			error : function(){
				$("#msg").show();
				$("#msg").html("Error on sales search");
			}
		});
		break;
		
	case "get":
		$.ajax({
			type : "post",
			headers: {"X-CSRF-TOKEN": token}, //send CSRF token in header
			url : "/sales/get",
			data: "id=" + $("#id").val(),
			success : function(res){
				if (res== null || res==""){
					$("#msg").show();
					$("#msg").html("Record not found")
				}else{
					$("#id").val(res.id);
					$("#item").val(res.item);
					$("#quantity").val(res.quantity);
					$("#price").val(res.price);
					$("#sale_date").val(res.sale_date);
				}
			},
			error : function(){
				$("#msg").show();
				$("#msg").html("Error on sales search");
			}
		});
		break;
	case "insert":
		var json = 
	{
			
			'item' : $("#item").val(),
			'quantity' : $("#quantity").val(),
			'price' : ($("#price").val() ? $("#price").val() : null),
			'sale_date' : ($("#sale_date").val() ? $("#sale_date").val() : null)
	}
		var postData= JSON.stringify(json);
		
		$.ajax({
			type : "post",
			headers: {"X-CSRF-TOKEN": token}, //send CSRF token in header
			url : "/sales/insert",
			data: postData,
			contentType : "application/json; charset=utf-8",
			success : function(res){
				if (res== 1){
					$("#msg").removeClass("alert-danger").addClass("alert-success")
					$("#msg").show();
					$("#msg").html("Record has been inserted correctly")
				}else{
					$("#msg").show();
					$("#msg").html("Error adding record")
				}
			},
			error : function(){
				$("#msg").show();
				$("#msg").html("Error adding record");
			}
		});
		break;
		
	case "update":
		var json = 
	{
			'id' : $("#id").val(),
			'item' : $("#item").val(),
			'quantity' : $("#quantity").val(),
			'price' : ($("#price").val() ? $("#price").val() : null),
			'sale_date' : ($("#sale_date").val() ? $("#sale_date").val() : null)
	}
		var postData= JSON.stringify(json);
		
		$.ajax({
			type : "post",
			headers: {"X-CSRF-TOKEN": token}, //send CSRF token in header
			url : "/sales/update",
			data: postData,
			contentType : "application/json; charset=utf-8",
			success : function(res){
				if (res== 1){
					$("#msg").removeClass("alert-danger").addClass("alert-success")
					$("#msg").show();
					$("#msg").html("Record has been updated correctly")
				}else{
					$("#msg").show();
					$("#msg").html("Error updating record")
				}
			},
			error : function(){
				$("#msg").show();
				$("#msg").html("Error updating record");
			}
		});
		break;
		
	case "delete":
		$.ajax({
			type : "post",
			headers: {"X-CSRF-TOKEN": token}, //send CSRF token in header
			url : "/sales/delete",
			data: "id=" + $("#id").val(),
			success : function(res){
				if (res==1){
					$("#msg").removeClass("alert-danger").addClass("alert-success")
					$("#msg").show();
					$("#msg").html("Record deleted")
				}else{
					$("#msg").show();
					$("#msg").html("Record could not be deleted")
				}
			},
			error : function(){
				$("#msg").show();
				$("#msg").html("Error on delete");
			}
		});
		break;
	default:
			$("#msg").show();
			$("#msg").html("Invalid option");
	}
}